#include "node.h"
#include<bits/stdc++.h>
using namespace std;

// Node class
Node::Node(int key, int level)
{
    this->key = key;
    this->level=level;
    // allocate memory to next and previous nodes
    next = new Node *[level + 1];
    prev = new Node *[level + 1];
    // Fill next and previous array with 0(NULL)
    memset(next, 0, sizeof(Node *) * (level + 1));
    memset(prev, 0, sizeof(Node *) * (level + 1));
}