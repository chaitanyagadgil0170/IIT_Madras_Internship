#include "doubly_skip_list.h"
#include<bits/stdc++.h>
using namespace std;

// skip list constructor 
skip_list::skip_list(int MAXLVL, float p)
{
    this->MAXLVL = MAXLVL;  
    this->p = p;
    level = 0;
    header = new Node(INT_MIN, MAXLVL); // assign header node with the INT_MIN value
    tail = new Node(INT_MAX, MAXLVL);  // assign tail node with the INT_MAX value
    for (int i = MAXLVL; i >= 0; i--)
    {
        // connect head and tail
        header->next[i] = tail; 
        header->prev[i] = NULL;
        tail->next[i] = NULL;
        tail->prev[i] = header;
        level_median[i] = header; // set level wise median to header node value inititally 
    }
}

// random level calculator function 
int skip_list::random_level()
{
    float r = (float)rand() / RAND_MAX;
    int lvl = 0;
    while (r < p && lvl < MAXLVL)
    {
        lvl++;
        r = (float)rand() / RAND_MAX;
    }
    return lvl;
}

//function to create new node 
Node *skip_list::create_node(int key, int random_level)
{
    Node *n = new Node(key, random_level);
    return n;
}


void skip_list::insert_fixed_level(int key,int given_lev)
{
    //inserts node at a pre-defined level 
    Node *n = create_node(key, given_lev);
    Node *ptr;
    if (given_lev > level)
    {
        level = given_lev;
    }
    if (key > level_median[level]->key)
    {
        ptr = tail;
    }
    else
    {
        ptr = header;
    }
    // compare key with level median, if greater then set the pointer to tail and back traverse, if lesser traverse forward
    for (int i = level; i >= 0; i--)
    {
        if (key > level_median[i]->key) 
        {
            while (ptr->prev[i] && ptr->prev[i]->key > key)
            {
                ptr = ptr->prev[i];
            }
            if (given_lev >= i && ptr->key != key)
            {
                n->prev[i] = ptr->prev[i];
                n->next[i] = ptr;
                ptr->prev[i] = n;
                n->prev[i]->next[i] = n;
            }
        } // backwards traversal
        else 
        {
            while (ptr->next[i] && ptr->next[i]->key < key)
            {
                ptr = ptr->next[i];
            }
            if (given_lev >= i && ptr->key != key)
            {
                n->next[i] = ptr->next[i];
                n->prev[i] = ptr;
                ptr->next[i] = n;
                n->next[i]->prev[i] = n;
            }
        }
    }
}

void skip_list::insert_element(int key)
{
    //insertion for random level 
    int random_lev = random_level();
    Node *n = create_node(key, random_lev);
    Node *ptr;
    if (random_lev > level)
    {
        level = random_lev;
    }
    if (key > level_median[level]->key)
    {
        ptr = tail;
    }
    else
    {
        ptr = header;
    }
    for (int i = level; i >= 0; i--)
    {
        if (key > level_median[i]->key)
        {
            while (ptr->prev[i] && ptr->prev[i]->key > key)
            {
                ptr = ptr->prev[i];
            }
            if (random_lev >= i && ptr->key != key)
            {
                n->prev[i] = ptr->prev[i];
                n->next[i] = ptr;
                ptr->prev[i] = n;
                n->prev[i]->next[i] = n;
            }
        } // backwards traversal
        else 
        {
            while (ptr->next[i] && ptr->next[i]->key < key)
            {
                ptr = ptr->next[i];
            }
            if (random_lev >= i && ptr->key != key)
            {
                n->next[i] = ptr->next[i];
                n->prev[i] = ptr;
                ptr->next[i] = n;
                n->next[i]->prev[i] = n;
            }
        }
    }
}


int skip_list::search(int key)
{
    int no_of_steps = 0;
    Node *ptr;
    if (key > level_median[level]->key)
    {
        ptr = tail;
    }
    else
    {
        ptr = header;
    }
    // compare key with level median, if greater then set the pointer to tail and back traverse, if lesser traverse forward
    for (int i = level; i >= 0; i--)
    {
        if (key == level_median[i]->key)
        {
            cout << "Element found, steps count = " << no_of_steps << endl;
            return no_of_steps;
        }
        else if (key > level_median[i]->key)
        {
            while (ptr->prev[i] && ptr->prev[i]->key >= key && ptr->prev[i]->key != level_median[i]->key)
            {
                ptr = ptr->prev[i];
                cout << ptr->key << "  ";
                no_of_steps++;
            }
        } // backwards traversal
        else
        { // forward traversal
            while (ptr->next[i] && ptr->next[i]->key <= key && ptr->next[i]->key != level_median[i]->key)
            {
                ptr = ptr->next[i];
                cout << ptr->key << "  ";
                no_of_steps++;
            }
        }

        if (ptr->key == key)
        {
            cout << "Element found, steps count = " << no_of_steps << endl;
            return no_of_steps;
        }
    }
    // cout << ptr->key << endl;
    if (ptr && ptr->key == key)
    {
        cout << " Element found, steps count = " << no_of_steps << endl;
    }
    else
        cout << "Element not found\n";
        return no_of_steps;
}

void skip_list::delete_element(int key)
{
    Node *ptr;
    int max_key_lev = level;
      // compare key with level median, if greater then set the pointer to tail and back traverse, if lesser traverse forward
    if (key > level_median[level]->key)
    {
        ptr = tail;
    }
    else
    {
        ptr = header;
    }
    for (int i = level; i >= 0; i--)
    {
        if (key == level_median[i]->key)
        {
            ptr = level_median[i];
            max_key_lev = i;
            break;
        }
        else if (key > level_median[i]->key || (ptr->key < level_median[i]->key && ptr->key > key))
        { // backwards traversal
            while (ptr->prev[i] && ptr->prev[i]->key >= key && ptr->prev[i]->key != level_median[i]->key)
            {
                ptr = ptr->prev[i];
            }
        }
        else
        { // forward traversal
            while (ptr->next[i] && ptr->next[i]->key <= key && ptr->next[i]->key != level_median[i]->key)
            {
                ptr = ptr->next[i];
            }
        }
        if (ptr->key == key)
        {
            cout << "key  is  " << endl;
            max_key_lev = i;
            break;
        }
    }
    cout << ptr->key << endl;
    cout << max_key_lev << endl;
    if (ptr and ptr->key == key)
    {
        for (int i = max_key_lev; i >= 0; i--)
        {
            // remove the element and change pointers 
            ptr->next[i]->prev[i] = ptr->prev[i];
            ptr->prev[i]->next[i] = ptr->next[i];
            cout << level_median[i]->key << "  ";
        }
        while (level > 0 && header->next[level] == tail)
            level--;
            //decrease the level if only a single element at topmost level
    }
    else
    {
        cout << "Element does not exist in the list." << endl;
    }
}

void skip_list::display()
{
    cout << "\n***** Doubly Skip List*****"
         << "\n";
    for (int i = 0; i<= level; i++)
    {
        Node *node = header->next[i];
        cout << "Level " << i << ": ";
        Node *prev = header;
        while (node != tail)
        {
            cout << node->key<<"  ";
            node = node->next[i];
        }
        cout<< "   " << level_median[i]->key;
        cout << "\n";
    }
    cout<<header->next[0]->next[3]<<endl;
}

void skip_list::set_medians(){
    // set medians level wise 
    Node *node = header->next[0];
    int count=0;
    //count nodes
    while (node != tail)
        {
            cout << node->key << "  ";
            node = node->next[0];
            count++;
        }
    // find median position
    count/=2;
    int count1=count;
    node=header->next[0];
    while(count--){
        node = node->next[0];
    }
    int i;
    int tmp = node->key;
    int tmp_level = node->level;
    int cur_level = level;
    int top_lev_key;
    // find median and push it to the top level, remove any arbitrary top level element to maintain uniformity in the skip list by swapping the levels of this median and the top level element 
    top_lev_key = header->next[level]->key;
    delete_element(header->next[level]->key);
    insert_fixed_level(top_lev_key,tmp_level);
    delete_element(tmp);
    insert_fixed_level(tmp,cur_level);
    display();
    node = header->next[0];
    while(count1--){
        node = node->next[0];
    }
    // assign level-wise medians
    for(int i=0;i<=level;i++){
        level_median[i]=node;
    }
}

void skip_list::get_data(){
    int choice, ele;
    int no_of_operations=0;
    int batch_size=40000;
    fstream fout;
    fout.open("results_doubly_skipll_40000.csv", ios::out | ios::app);
    clock_t t;
    do
    {
        cout << "\n1)Insert\n2)Delete\n3)Search\n4)Exit\nEnter your choice: ";
        cin >> choice;
        switch (choice)
        {
        case 1:
            cout << "Enter element: ";
            cin >> ele;
            for (int i = 1; i < 400000; i++)
            {
                int el = rand()%5000;
                insert_element(i);
                no_of_operations++;
                if(no_of_operations%batch_size==0) set_medians();
            }
            for (int i = 1; i < 400000; i++)
            {
                fout<<i<<","<<search(i)<<"\n";
            }
            display();
            break;
        case 2:
            cout << "Enter element: ";
            cin >> ele;
            delete_element(ele);
            no_of_operations++;
            display();
            break;
        case 3:
            cout << "Enter element: ";
            cin >> ele;
            search(ele);
            break;
        default:
            break;
        }
        if(no_of_operations%batch_size==0) set_medians();
    } while (choice != 5);
}
