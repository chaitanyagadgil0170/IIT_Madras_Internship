#include<bits/stdc++.h>
#include "./doubly_skip_list.h"
using namespace std;
int main()
{
    int n;
    cout<<"Enter the maximum number of elements you would like to create a skip list for:"<<endl;
    cin>>n;
    double lev = log2(n)+1;
    skip_list sk_lt(int(lev), 0.5);
    sk_lt.get_data();
    return 0;
}