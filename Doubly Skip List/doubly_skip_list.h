#ifndef DOUBLY_SKIP_LIST_H
#define DOUBLY_SKIP_LIST_H
#include "./node.h"

// SKip list class
class skip_list
{
    int MAXLVL; // maximum level for the doubly skip list 
    int level;  // current maximum level
    float p; // fraction of nodes per level 
    Node *header; // header node 
    Node *tail; // tail node

public:
    Node **level_median;
    skip_list(int MAXLVL, float p);
    int random_level();
    Node *create_node(int key, int random_level);
    void insert_element(int key);
    void insert_fixed_level(int key,int level);
    void delete_element(int key);
    int search(int key);
    void display();
    void get_data();
    void set_medians();
};
#endif