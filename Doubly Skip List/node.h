#ifndef NODE_H
#define NODE_H

//node class
class Node
{
public:
    int key;
    int level;
    Node **next;
    Node **prev;
    Node(int, int);
};
#endif