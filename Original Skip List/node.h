#ifndef NODE_H
#define NODE_H

class Node
{
    public:
        int key;
        Node **forward;
        Node(int, int);
};

#endif