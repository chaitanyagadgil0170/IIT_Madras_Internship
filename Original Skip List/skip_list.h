#ifndef SKIP_LIST_H
#define SKIP_LIST_H
#include "node.h"

class skip_list 
{ 
    int MAXLVL; 
    float P; 
    int level; 
    Node *header;
    
    public: 
        skip_list(int,float); 
        int random_level(); 
        Node* create_node(int, int); 
        void insert_element(int); 
        void delete_element(int);
        int search(int);
        void display(); 
        void getdata();
}; 

#endif
