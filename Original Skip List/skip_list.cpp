#include "skip_list.h"
#include<bits/stdc++.h>
using namespace std;

skip_list::skip_list(int MAXLVL,float P) 
{ 
    this->MAXLVL = MAXLVL; 
    this->P = P; 
    level = 0; 
    header = new Node(-1, MAXLVL); 
} 

int skip_list::random_level() 
{ 
    float r = (float)rand()/RAND_MAX;
    int lvl = 0;
    while(r < P && lvl < MAXLVL)
    {
        lvl++;
        r = (float)rand()/RAND_MAX;
    }
    return lvl;
} 

Node* skip_list::create_node(int key, int rlevel) 
{ 
    Node *n = new Node(key, rlevel); 
    return n; 
}

void skip_list::insert_element(int key)
{
    Node* current = header;
    int rlevel = random_level();
    Node* prev = NULL;
    Node* n = create_node(key, rlevel);

    if(rlevel>level){
        level = rlevel;
    }

    for (int i = level; i >= 0; i--) 
    { 
        while(current->forward[i] != NULL && current->forward[i]->key <= key){
            current = current->forward[i];
        }
        if(rlevel >= i && current->key != key){
            n->forward[i] = current->forward[i];
            current->forward[i] = n;
        }
    }
}

int skip_list::search(int key){
    Node* current = header;
    int no_of_steps=0;
    for(int i=level;i>=0;i--){
        while(current->forward[i] && current->forward[i]->key<=key){
            current = current->forward[i];cout<<current->key<<"  ";
            no_of_steps++;
        }
        if(current->key==key){cout<<"Element found, steps count = "<<no_of_steps<<endl;
        return no_of_steps; }
    }
    current = current->forward[0];
    if(current && current->key==key){
        cout<<"Found , steps count = "<<no_of_steps<<endl;
    }
    else
        cout<<"Not found\n";
    return no_of_steps;
}

void skip_list::delete_element(int key){
    Node* current = header;
    Node* update[MAXLVL+1];
    memset(update, 0, sizeof(Node*)*(MAXLVL+1)); 

    for(int i=level;i>=0;i--){
        while(current->forward[i] && current->forward[i]->key<key){
            current = current->forward[i];
        }
        update[i] = current;
    }   

    current = current->forward[0];
     
    if(current != NULL and current->key == key){
        for(int i=0;i<=level;i++){ 
            if(update[i]->forward[i] != current) 
                break; 
            update[i]->forward[i] = current->forward[i]; 
        } 
    }

    while(level>0 && header->forward[level] == 0) 
        level--; 
}

void skip_list::display() 
{ 
    cout<<"\n*****Skip List*****"<<"\n"; 
    for (int i=0;i<=level;i++) 
    { 
        Node *node = header->forward[i]; 
        cout << "Level " << i << ": "; 
        while (node != NULL) 
        { 
            cout << node->key<<" "; 
            node = node->forward[i]; 
        } 
        cout << "\n"; 
    } 
}

void skip_list::getdata(){
    int choice,ele;
    clock_t t;
    fstream fout;
    fout.open("results_skipll.csv", ios::out | ios::app);
    do{
        cout<<"\n1)Insert\n2)Delete\n3)Search\n4)Exit\nEnter your choice: ";
        cin>>choice;
        switch(choice){
            case 1: for(int i=1;i<400000;i++)
                    {insert_element(i);}
                    for (int i = 1; i < 400000; i++)
                    {
                    fout<<i<<","<<search(i)<<"\n";
                    }
                    display();
                    break;
            case 2: cout<<"Enter element: ";
                    cin>>ele;
                    delete_element(ele);
                    display();
                    break;
            case 3: cout<<"Enter element: ";
                    cin>>ele;
                    search(ele);
                     break;
            case 4: exit(0);
        }
    }while(choice!=5);
}
